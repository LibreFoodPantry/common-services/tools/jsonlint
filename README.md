# jsonlint

A validator and reformatter for JSON
[jsonlint](https://jsonlint.com/).

To enable shellcheck in your project's pipeline add `jsonlint` to the `ENABLE-JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  DISABLE_JOBS: "build, test"
  ENABLE_JOBS: "json"
```

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
